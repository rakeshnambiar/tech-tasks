# tech-tasks

## how to run the program and the unit tests

`mvn clean test -Dmaven.test.skip=false`

## how to generate the html test-report

`junit-viewer --results=target/surefire-reports --save=unit_test_report.html`

## where will be the report stored?

`target` folder and the name will be `unit_test_report.html`

