import java.util.logging.Logger;

public class fifo_queue {
    final static Logger logger = Logger.getLogger(String.valueOf(fifo_queue.class));

    int def_queue_length;
    int[] fifo_queue;
    int front = 0;
    int rear = 0;
    int queue_size = 0;


    /*Constructor*/
    public fifo_queue(int length) {
        def_queue_length = length;
        fifo_queue = new int[def_queue_length];
    }

    public int getQueueMaxLength() throws Exception {
        try {
            return def_queue_length;
        } catch (Exception var2) {
            throw new Exception("ERROR: While getting the default FIFO Queue length");
        }
    }

    public int getQueueSize() throws Exception {
        try {
            return this.queue_size;
        } catch (Exception var2) {
            throw new Exception("ERROR: While getting the FIFO Queue length");
        }
    }

    public boolean isEmpty() throws Exception {
        try{
            return queue_size == 0;
        }catch (Exception e) {
            throw new Exception("ERROR: While verifying the FIFO Queue is empty");
        }
    }

    public boolean isFull() throws Exception {
        try{
            return queue_size == def_queue_length;
        }catch (Exception e) {
            throw new Exception("ERROR: While verifying the FIFO Queue is full");
        }
    }

    /*EnQueue*/
    public void add(int data) throws Exception {
        try{
            if(!isFull()) {
                fifo_queue[rear] = data;
                rear = (rear + 1)%def_queue_length;
                queue_size++;
            } else {
                logger.warning("Queue is full. Unable to EnQueue the value - " + data);
            }
        }catch (Exception e){
            throw new Exception("ERROR: While Adding number to the FIFO Queue - " + data);
        }
    }

    /*DeQueue*/
    public int get() throws Exception {
        int removed_value = 0;
        try{
            if(!isEmpty()) {
                removed_value = fifo_queue[front];
                front = (front + 1)%def_queue_length;
                queue_size--;
                logger.info("Number removed from the FIFO queue : " + removed_value);
            } else {
                logger.warning("Queue is empty");
            }
        }catch (Exception e){
            throw new Exception("ERROR: While getting number from the FIFO Queue");
        }
        return removed_value;
    }

    public void printFifoQueue() throws Exception {
        try{
            System.out.println("Fifo Queue values : ");
            for (int num=0; num < queue_size; ++num) {
                System.out.print(fifo_queue[(front + num)%def_queue_length] + " ");
            }
            System.out.println();
        }catch (Exception e){
            throw new Exception("ERROR: While printing the FIFO queue values");
        }
    }

}
