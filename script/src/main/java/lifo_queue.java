import java.util.logging.Logger;

public class lifo_queue {
    final static Logger logger = Logger.getLogger(String.valueOf(lifo_queue.class));

    int def_queue_length;
    int[] lifo_queue;
    int top = 0;

    /*Constructor*/
    public lifo_queue(int length) {
        def_queue_length = length;
        lifo_queue = new int[def_queue_length];
    }

    public int getQueueMaxLength() throws Exception {
        try {
            return def_queue_length;
        } catch (Exception var2) {
            throw new Exception("ERROR: While getting the default LIFO Queue length");
        }
    }

    public int getQueueSize() throws Exception {
        try {
            return this.top;
        } catch (Exception var2) {
            throw new Exception("ERROR: While getting the LIFO Queue length");
        }
    }

    public boolean isEmpty() throws Exception {
        try{
            return top == 0;
        }catch (Exception e) {
            throw new Exception("ERROR: While verifying the LIFO Queue is empty");
        }
    }

    public boolean isFull() throws Exception {
        try{
            return top == def_queue_length;
        }catch (Exception e) {
            throw new Exception("ERROR: While verifying the LIFO Queue is full");
        }
    }

    /*To push a new value to the queue*/
    public void add(int data) throws Exception {
        try{
            if(isFull()) {
                logger.warning("LIFO queue is full. Unable to push the value - " + data);
            } else {
                lifo_queue[top] = data;
                top++;
            }
        }catch (Exception e){
            throw new Exception("ERROR: While Pushing the Item to the LIFO queue");
        }
    }

    /*To pop a new value from the queue*/
    public int get() throws Exception {
        int removed_value = 0;
        try {
            if (isEmpty()) {
                logger.warning("The LIFO queue is empty");
            } else {
                removed_value = lifo_queue[top-1];
                top--;
                logger.info("Number removed from the LIFO queue : " + removed_value);
            }
        } catch (Exception e){
            throw new Exception("ERROR: While removing the value from LIFO queue");
        }
        return removed_value;
    }


    public void printLifoQueue() throws Exception {
        try{
            System.out.print("Lifo Queue values : ");
            for (int num=top-1; num >= 0; --num) {
                System.out.print(lifo_queue[num] + " ");
            }
            System.out.println();
        }catch (Exception e){
            throw new Exception("ERROR: While printing the value of LIFO queue");
        }
    }
}
