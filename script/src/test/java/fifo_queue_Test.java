import org.junit.*;


public class fifo_queue_Test {
    int queue_length = 5;

    private fifo_queue fifo_test;

    @Before
    public void initializeTest() {
        fifo_test = new fifo_queue(queue_length);
    }

    @Test
    public void verifyDefaultQueueLength() throws Exception {
        Assert.assertTrue("Assertion Error: Incorrect default Queue length",
                fifo_test.getQueueMaxLength() == queue_length);
    }

    @Test
    public void verifyAdd() throws Exception {
        fifo_test.add(10);
        fifo_test.add(20);
        fifo_test.add(30);
        Assert.assertTrue("Assertion Error: Incorrect FIFO Queue size after adding the values",
                fifo_test.getQueueSize() == 3);
        fifo_test.printFifoQueue();
    }

    @Test
    public void verifyGet() throws Exception {
        fifo_test.add(5);
        fifo_test.add(2);
        fifo_test.add(8);
        fifo_test.add(7);
        Assert.assertTrue("Assertion Error: Broken DeQueue feature", fifo_test.get()==5);
        Assert.assertTrue("Assertion Error: Incorrect Queue size", fifo_test.getQueueSize() == 3);
        fifo_test.add(9);
        Assert.assertTrue("Assertion Error: Incorrect Queue size", fifo_test.getQueueSize() == 4);
        fifo_test.printFifoQueue();
    }

    @Test
    public void verifyIsEmpty() throws Exception {
        Assert.assertTrue("Assertion Error: Check failed when the FIFO Queue is empty", fifo_test.isEmpty());
        fifo_test.add(9);
        fifo_test.get();
        Assert.assertTrue("Assertion Error: Check failed when the FIFO Queue EnQueue and DeQueued", fifo_test.isEmpty());
        fifo_test.add(1);
        Assert.assertFalse("Assertion Error: Check failed when the FIFO Queue is NOT empty", fifo_test.isEmpty());
    }

    @Test
    public void verifyIsFull() throws Exception {
        fifo_test.add(5000);
        fifo_test.add(2000);
        fifo_test.add(8000);
        Assert.assertFalse("Assertion Error: Check failed when the FIFO Queue is NOT full", fifo_test.isFull());
        fifo_test.add(7000);
        fifo_test.add(9000);
        Assert.assertTrue("Assertion Error: Check failed when the FIFO Queue is full", fifo_test.isFull());
    }

    @Test
    public void verifyNegNumberQueue() throws Exception {
        fifo_test.add(-2);
        fifo_test.add(-3);
        fifo_test.printFifoQueue();
        Assert.assertTrue("Assertion Error: Failed the negative queue check", fifo_test.queue_size==2);
    }

    @Test
    public void verifyAddingValuesMoreThanLimit() throws Exception {
        fifo_test.add(500);
        fifo_test.add(200);
        fifo_test.add(800);
        fifo_test.add(700);
        fifo_test.add(0);
        fifo_test.add(300);
        Assert.assertTrue("Assertion Error: Check failed when the FIFO Queue is full and add mre value",
                    fifo_test.getQueueSize()==this.queue_length);
    }

    @Test
    public void verifyEmptyQueueRemoval() throws Exception {
        Assert.assertTrue("Assertion Error: Check failed while removing value from empty FIFO queue", fifo_test.get() == 0);
    }
}
