import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class lifo_queue_Test {

    int queue_length = 4;

    private lifo_queue lifo_test;

    @Before
    public void initializeTest() {
        lifo_test = new lifo_queue(queue_length);
    }

    @Test
    public void verifyDefaultQueueLength() throws Exception {
        Assert.assertTrue("Assertion Error: Incorrect default LIFO Queue length",
                lifo_test.getQueueMaxLength() == queue_length);
    }

    @Test
    public void verifyAdd() throws Exception {
        lifo_test.add(10);
        lifo_test.add(20);
        lifo_test.add(30);
        lifo_test.printLifoQueue();
        Assert.assertTrue("Assertion Error: Incorrect LIFO Queue size after adding values",
                lifo_test.getQueueSize()==3);
    }
    @Test
    public void verifyGet() throws Exception {
        lifo_test.add(5);
        lifo_test.add(2);
        lifo_test.add(8);
        lifo_test.add(7);
        Assert.assertTrue("Assertion Error: Broken DeQueue feature", lifo_test.get()==7);
        Assert.assertTrue("Assertion Error: Incorrect Queue size", lifo_test.getQueueSize() == 3);
        lifo_test.add(9);
        Assert.assertTrue("Assertion Error: Incorrect Queue size", lifo_test.getQueueSize() == 4);
        lifo_test.printLifoQueue();
    }

    @Test
    public void verifyIsEmpty() throws Exception {
        Assert.assertTrue("Assertion Error: Check failed when the LIFO Queue is empty", lifo_test.isEmpty());
        lifo_test.add(9);
        lifo_test.get();
        Assert.assertTrue("Assertion Error: Check failed when the LIFO Queue EnQueue and DeQueued", lifo_test.isEmpty());
        lifo_test.add(1);
        Assert.assertFalse("Assertion Error: Check failed when the LIFO Queue is NOT empty", lifo_test.isEmpty());
    }

    @Test
    public void verifyIsFull() throws Exception {
        lifo_test.add(5000);
        lifo_test.add(2000);
        lifo_test.add(8000);
        Assert.assertFalse("Assertion Error: Check failed when the LIFO Queue is NOT full", lifo_test.isFull());
        lifo_test.add(7000);
        lifo_test.add(9000);
        Assert.assertTrue("Assertion Error: Check failed when the LIFO Queue is full", lifo_test.isFull());
    }

    @Test
    public void verifyNegNumberQueue() throws Exception {
        lifo_test.add(-2);
        lifo_test.add(-3);
        lifo_test.printLifoQueue();
        Assert.assertTrue("Assertion Error: Failed the negative queue check", lifo_test.getQueueSize()==2);
    }

    @Test
    public void verifyAddingValuesMoreThanLimit() throws Exception {
        lifo_test.add(500);
        lifo_test.add(200);
        lifo_test.add(800);
        lifo_test.add(0);
        lifo_test.add(300);
        Assert.assertTrue("Assertion Error: Check failed when the LIFO Queue is full and add mre value",
                lifo_test.getQueueSize()==this.queue_length);
    }

    @Test
    public void verifyEmptyQueueRemoval() throws Exception {
        Assert.assertTrue("Assertion Error: Check failed while removing value from empty LIFO queue", lifo_test.get() == 0);
    }
}
